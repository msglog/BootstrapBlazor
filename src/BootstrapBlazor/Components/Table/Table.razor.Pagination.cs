﻿// Copyright (c) Argo Zhang (argo@163.com). All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// Website: https://www.blazor.zone or https://argozhang.github.io/

namespace BootstrapBlazor.Components;

public partial class Table<TItem>
{
    /// <summary>
    /// 获得/设置 是否分页 默认为 false
    /// </summary>
    [Parameter]
    public bool IsPagination { get; set; }

    /// <summary>
    /// 获得/设置 是否在顶端显示分页 默认为 false
    /// </summary>
    [Parameter]
    public bool ShowTopPagination { get; set; }

    /// <summary>
    /// 获得/设置 是否显示行号列 默认为 false
    /// </summary>
    [Parameter]
    public bool ShowLineNo { get; set; }

    /// <summary>
    /// 获得/设置 行号列标题文字 默认为 行号
    /// </summary>
    [Parameter]
    [NotNull]
    public string? LineNoText { get; set; }

    /// <summary>
    /// 获得/设置 每页显示数据数量的外部数据源
    /// </summary>
    [Parameter]
    [NotNull]
    public IEnumerable<int> PageItemsSource { get; set; } = new int[] { 20, 50, 100, 200, 500, 1000 };

    /// <summary>
    /// 获得/设置 默认每页数据数量 默认 null 使用 <see cref="PageItemsSource"/> 第一个值
    /// </summary>
    /// <remarks>此参数仅首次加载时生效</remarks>
    [Parameter]
    public int? PageItems { get; set; }

    /// <summary>
    /// 异步查询回调方法，设置 <see cref="Items"/> 后无法触发此回调方法
    /// </summary>
    [Parameter]
    public Func<QueryPageOptions, Task<QueryData<TItem>>>? OnQueryAsync { get; set; }

    /// <summary>
    /// 获得/设置 数据总条目
    /// </summary>
    protected int TotalCount { get; set; }

    /// <summary>
    /// 获得/设置 当前页码
    /// </summary>
    protected int PageIndex { get; set; } = 1;

    /// <summary>
    /// 当前分页组件每页显示数量
    /// </summary>
    protected int CurrentPageItems { get; set; }

    /// <summary>
    /// 获得/设置 当前行
    /// </summary>
    protected int StartIndex { get; set; }

    /// <summary>
    /// 点击页码调用此方法
    /// </summary>
    /// <param name="pageIndex"></param>
    /// <param name="pageItems"></param>
    protected async Task OnPageClick(int pageIndex, int pageItems)
    {
        if (pageIndex != PageIndex)
        {
            SelectedRows.Clear();
            await OnSelectedRowsChanged();
            PageIndex = pageIndex;
            CurrentPageItems = pageItems;
            await QueryAsync();
        }
    }

    /// <summary>
    /// 每页记录条数变化是调用此方法
    /// </summary>
    protected async Task OnPageItemsChanged(int pageItems)
    {
        if (CurrentPageItems != pageItems)
        {
            PageIndex = 1;
            CurrentPageItems = pageItems;
            await QueryAsync();
        }
    }
}
